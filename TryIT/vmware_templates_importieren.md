# 7zip
Archivpasswort: `Banane22`
Das Archiv kann mit 7zip entpackt werden.

# VMWare Templates Importieren
Die VMWare Templates können wie folgt Importiert werden:
1. Doppelklick auf das `.ovf` File
2. Den VMWare Wizard durchklicken


## Wichtig!!!
Es ist darauf acht zu geben, dass jede Virtuelle Maschiene sich im VMNet08 und nicht im standard NAT befindet.
![VMWare Network Setup 2][vmware_vmnetwork_configuration.png]


## VM's Starten
Nun können alle Virtuellen Maschienen gestartet werden.







[vmware_vmnetwork_configuration.png]: ../assets/img/vmware_vmnetwork_configuration.png "VMWare Network Setup"
