# Repository Klonen

Als erstes loggen wir uns auf der Ansible VM ein. Dies kann bequem mit Putty auf die Adresse `192.192.168.79.60` gemacht werden.
<br>
User: `root` <br>
Passwort: `Banane22`


Wir Klonen das Repository in die Ansible VM um den Aktuellsten stand zu erhalten.
```bash
git clone https://BigGold1310@gitlab.com/BigGold1310/TBZ_OpenShift_Cluster_with_Ansible.git
```


Nun müssen wir noch das OpenShift Repository Klonen. Hier Checken wir die Version 1.5 aus.
```bash
git clone https://github.com/openshift/openshift-ansible
cd openshift-ansible
git checkout release-1.5
cd ..
```
