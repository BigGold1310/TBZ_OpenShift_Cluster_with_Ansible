# Demo App mit CLI und GUI

## Login on master

Als erstes erstellen wir ein Projekt.
```
oc new-project test
```
In diesem Projekt legen wir verschiedene Apps an.
```
oc new-app centos/ruby-22-centos7~https://github.com/openshift/ruby-hello-world.git
```
Die Apps sogennant Pods können mit fogendem Befehl angezeigt werden.
```
oc get pods
```
Mitels des svc Befehles kann man sich die aus den Apps entstehenden Services anzeigen lassen.
```
oc get svc
```


Mit folgendem Befehl können wier wieder den Service Testen. Achtung IP anpassen!
```
curl -s 172.30.140.4:8080|head -n 20
```


Nun müssten wir nur noch eine Route auf dem OpenShift Router einrichten, dass der Service von aussen Erreichbar ist.
