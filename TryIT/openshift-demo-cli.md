# CLI Hello World Beispiel

Einen Pod erstellen
```
oc create -f examples/hello-openshift/hello-pod.json
```

IP aus dem Pod auslesen
```
oc get pod hello-openshift -o yaml |grep podIP
 podIP: 10.1.0.2
```

Request an den Pod senden
```
$ curl 10.1.0.2:8080
 Hello OpenShift!
```


Den Pod Löschen
```
oc delete pod hello-openshift
```
