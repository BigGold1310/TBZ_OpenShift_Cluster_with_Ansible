# VMWare Network Settings
Vor dem Importieren der VMWare Templates müssen wir das Netzwerk richtig konfigurieren.

![VMWare Network Setup 1][vmware_network_setup_1.png]
![VMWare Network Setup 2][vmware_network_setup_2.png]
Hier ist die Konfiguration des `VMnet8` wichtig, dass die Subnet IP `192.168.79.0` und die Subnetzmaske `255.255.255.0` ist. Grund dafür ist, dass die VMWare Templates genau für dieses Netzwerk Konfiguriert wurden und dadurch nur mit dieser Konfiguration richtig laufen.

Hier gillt zu beachten, dass folgende IP Adressen frei sind:
```
192.168.79.60 -> Ansible
192.168.79.61 -> NFS/DNS
192.168.79.62 -> Master
192.168.79.63 -> Node1
192.168.79.64 -> Node2
```

## VMNet08
Wir konfigurieren gleich noch den DNS Server auf unserem Host Adapter. Dies wird später für den Web-zugriff auf den OpenShift Master verwendet.
![VMNet08 Host Adapter][vmnet08_host_adapter.png]

## Möglichkeit 2 (Nicht empfholen)
Man könnte in allen VM's sowie den Config Templates von Ansible alle IP Adressen anpassen. Für dieses Vorgehen sind erweiterte Linux Kentnisse erforderlich. Darum wird dies hier nicht erklärt.










[vmware_network_setup_1.png]: ../assets/img/vmware_network_setup_1.png "VMWare Network Setup"
[vmware_network_setup_2.png]: ../assets/img/vmware_network_setup_2.png "VMWare Network Setup"
[vmnet08_host_adapter.png]: ../assets/img/vmnet08_host_adapter.png "VMNet08 Host Adapter Configuration"
