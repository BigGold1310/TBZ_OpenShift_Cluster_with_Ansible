# Ansible Pre-Install
Hierfür gibt es ein seperates Ansible PlayBook. Dieses Installiert und Konfiguriert die Basis Software auf allen Servern welche nicht mit dem OpenShift PlayBook installiert wird.

Nun kann folgender Befehl ausgeführt werden:
```bash
 ansible-playbook -i TBZ_OpenShift_Cluster_with_Ansible/TryIT/ansible/inventories/pre-setup.ini TBZ_OpenShift_Cluster_with_Ansible/TryIT/ansible/playbooks/pre-setup/pre-setup.xml
```
Der Output sollte ungefähr so aussehen:
```
PLAY RECAP ******************************************************************************************************************************************************************************************************************************************************************************************************************
master                     : ok=22   changed=3    unreachable=0    failed=0
nfs                        : ok=17   changed=1    unreachable=0    failed=0
node1                      : ok=23   changed=2    unreachable=0    failed=0
node2                      : ok=23   changed=2    unreachable=0    failed=0

```
Wichtig ist nur, dass kein `Failed` Job existiert.
