# Einführung
Es lohnt sich erst diese Übersicht ganz durchzulesen und sich mit den Themen Ansible, Docker, OpenShift in der Theorie auseinander zu setzen.

## Logins
Das Login ist immer folgendes: <br>
User:       `root` <br>
Passwd:     `Banane22`


# VM Templates
* [VMWare Template Download](https://drive.google.com/drive/folders/0B5Mh4UB3MBqQQU1hYi1YX3BIRXM?usp=sharing)

# VMWare Workstation Pro Setup
* [Network Konfiguration](vmware_network_setup.md)

* [VM's Importieren](vmware_templates_importieren.md)

# OpenShift mit Ansible Installieren
Um diese Aufgaben zu erledigen müssen zwingend alle fünf VM's gestartet sein.
Die folgenden Tasks werden über Putty auf dem Ansible Host erledigt. (Mit Putty auf 192.168.79.60 verbinden)

* [Ansible Host Preparation](ansible-host-preperation.md)

Alle drei Installationsschritte zusammen benötigen etwa 15 Minuten.

* [Pre-install](ansible-pre-install.md)

* [Install OpenShift](ansible-install-openshift.md)

* [Post-Install](ansible-post-install.md)

Wenn alles richtig gemacht wurde ist das OpenShift Webinterface unter: https://master.openshift:8443/console erreichbar! :)


# Ausprobieren von OpenShift

## OpenShift Beispiel

* [OpenShift CLI Beispiel](openshift-demo-cli.md)

* [OpenShift Webinterface Beispiel](openshift-demo-gui-cli.md)
