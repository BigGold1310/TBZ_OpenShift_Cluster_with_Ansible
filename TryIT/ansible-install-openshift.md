# Ansible OpenShift Cluster installation
Mit dem OpenShift Playbook werden wir nun OpenShift Installieren.

Dies kann mit folgendem Befehl gemacht werden:
```bash
ansible-playbook -i TBZ_OpenShift_Cluster_with_Ansible/TryIT/ansible/inventories/openshift.ini openshift-ansible/playbooks/byo/config.yml
```

> Hinweis: Dieses Playbook dauert am längsten!

Wichtig ist nur, dass kein `Failed` Job existiert.
