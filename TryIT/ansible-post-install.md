# Ansible Post-Install
Hierfür gibt es wieder ein seperates Ansible PlayBook. Dieses macht die Endkonfiguration des Masters.

Dies kann mit folgendem Befehl gemacht werden:
```bash
ansible-playbook -i TBZ_OpenShift_Cluster_with_Ansible/TryIT/ansible/inventories/post-setup.ini  TBZ_OpenShift_Cluster_with_Ansible/TryIT/ansible/playbooks/post-setup/post-setup.xml
```
Wichtig ist nur, dass kein `Failed` Job existiert.

# Zusätzliche Benutzer erstellen

```bash
htpasswd -c TBZ_OpenShift_Cluster_with_Ansible/TryIT/ansible/playbooks/post-setup/files/htpasswd <USER>
New password: <PASSWORD HERE>
Re-type new password: <PASSWORD HERE>
Adding password for user <USER>
```
Danach kann man einfach den Ansible Post-Install Schritt wiederholen.
